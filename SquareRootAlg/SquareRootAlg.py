import termcolor
import os
import subprocess
import time
from CoolArt import Cool

def print_menu():
    print("Menu Options:")
    print("1. Calculate Square Root of 3 (Guess and Check Method)")
    print("2. Sick Cool Art")
    print("3. Quit The Program")
    print("\nPlease Choose An Option...")

os.system("cls")

while True:
    print_menu()

    options = [1, 2, 3]
    response = str(input())

    if response == str(1):
        StartGuess = int(1)
        while  StartGuess*StartGuess < int(3): #1.7320999999999194     #3.0001704099997206
            StartGuess += 0.0001
            print("Square Root of " + str(StartGuess*StartGuess) + " is " + str(StartGuess))

        else:
                break
        
    elif response == str(2):
        Cool()
        #print(termcolor.colored("\nNot yet done lol", 'red'))
    elif response == str(3):
        print("Quiting...")
        break
    else:
        print(termcolor.colored("\nChoose a number 1-3...", 'red'))
    